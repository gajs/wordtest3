﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WordTest3
{
    class Program
    {
        static void Main(string[] args)
        {
            WordHandler hdlr = new WordHandler();
            hdlr.InitDocument("NewTest.dotx");
            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.System));
            FileInfo[] allFiles = di.GetFiles("*.dll");
            allFiles = (from fi in allFiles
                        select fi).Take(60).ToArray();
            hdlr.FillTable(allFiles);
            Console.WriteLine("Document processing finished");
        }
    }
}
