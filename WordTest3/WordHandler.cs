﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

using Wd = Microsoft.Office.Interop.Word;

namespace WordTest3
{
    public class WordHandler
    {
		Wd.Application WordApp = new Wd.Application();
        Wd.Document ak01Doc = null;

        public WordHandler()
        {
            WordApp.Visible = true;
        }

        public void InitDocument(string templateName)
        {
			object fileName = Path.Combine(Environment.CurrentDirectory, templateName);
			object newTemplate = false;
			object docType = 0;
			object isVisible = true;
			ak01Doc = WordApp.Documents.Add(ref fileName, ref newTemplate, ref docType, ref isVisible);
			ak01Doc.Activate();
        }

        public void FillTable(FileInfo[] toPopulate)
        {
			Wd.Table table = null;
			object oEndOfDoc = "end_of_doc";
			object missing = System.Reflection.Missing.Value;

			Wd.Cell cell = null;
			Wd.Range range = null;

			object beforeRow = Type.Missing;
			object start = 0;
			object end = 0;
			object numRows = 0;
			object numColumns = 0;
			object mergeBeforeSplit = false;

            int txtIndex = 0; int bcIndex = 0;
            int pageNr = 1;
            bool splitTable = false;

            // Anbauteile
			try
			{
				table = ak01Doc.Tables.Add(ak01Doc.Bookmarks[ref oEndOfDoc].Range, 1, 5, ref missing, ref missing);

				cell = table.Cell(1, 1);
				cell.Width = 75;
				cell.Range.Text = "Name";
				cell.Range.Font.Name = "Arial";
				cell.Range.Font.Size = 10;
				cell.Range.Bold = 1;
				cell.Range.ParagraphFormat.Alignment = Wd.WdParagraphAlignment.wdAlignParagraphLeft;

				cell = table.Cell(1, 2);
				cell.Width = 80;
				cell.Range.Text = "Size";
				cell.Range.Font.Name = "Arial";
				cell.Range.Font.Size = 10;
				cell.Range.Bold = 1;
				cell.Range.ParagraphFormat.Alignment = Wd.WdParagraphAlignment.wdAlignParagraphLeft;

				cell = table.Cell(1, 3);
				cell.Width = 145;
				cell.Range.Text = "Directory";
				cell.Range.Font.Name = "Arial";
				cell.Range.Font.Size = 10;
				cell.Range.Bold = 1;
				cell.Range.ParagraphFormat.Alignment = Wd.WdParagraphAlignment.wdAlignParagraphLeft;

				cell = table.Cell(1, 4);
				cell.Width = 40;
				cell.Range.Text = "Count";
				cell.Range.Font.Name = "Arial";
				cell.Range.Font.Size = 10;
				cell.Range.Bold = 1;
				cell.Range.ParagraphFormat.Alignment = Wd.WdParagraphAlignment.wdAlignParagraphLeft;

				cell = table.Cell(1, 5);
				cell.Width = 185;
				cell.Range.Text = "Last changed on";
				cell.Range.Font.Name = "Arial";
				cell.Range.Font.Size = 10;
				cell.Range.Bold = 1;
				cell.Range.ParagraphFormat.Alignment = Wd.WdParagraphAlignment.wdAlignParagraphLeft;


				foreach (FileInfo fi in toPopulate)
				{
					Wd.Row row = table.Rows.Add(ref beforeRow);
                    // COM index are 1-based
					txtIndex = table.Rows.Count;
                    Debug.Print("Row {0} added", txtIndex);
                    if (row.Cells.Count < 5)
                    {
                        //split second cell in to four
                        numRows = 1;
                        numColumns = 4;
                        row.Cells[1].Split(ref numRows, ref numColumns);
                    }

					table.Cell(txtIndex, 1).Width = 75;
                    range = table.Cell(txtIndex, 1).Range;
                    range.Text = fi.Name;
					range.Font.Name = "Arial";
					range.Font.Size = 10;
					range.Bold = 0;
					table.Cell(txtIndex, 1).Range.ParagraphFormat.Alignment =
						Wd.WdParagraphAlignment.wdAlignParagraphLeft;

					table.Cell(txtIndex, 2).Width = 80;
					table.Cell(txtIndex, 2).Range.Text = fi.Length.ToString();
					table.Cell(txtIndex, 2).Range.Font.Name = "Arial";
					table.Cell(txtIndex, 2).Range.Font.Size = 10;
					table.Cell(txtIndex, 2).Range.Bold = 0;
					table.Cell(txtIndex, 2).Range.ParagraphFormat.Alignment =
						Wd.WdParagraphAlignment.wdAlignParagraphLeft;

					table.Cell(txtIndex, 3).Width = 145;
					table.Cell(txtIndex, 3).Range.Text = fi.Directory.Name;
					table.Cell(txtIndex, 3).Range.Font.Name = "Arial";
					table.Cell(txtIndex, 3).Range.Font.Size = 10;
					table.Cell(txtIndex, 3).Range.Bold = 0;
					table.Cell(txtIndex, 3).Range.ParagraphFormat.Alignment =
						Wd.WdParagraphAlignment.wdAlignParagraphLeft;

					table.Cell(txtIndex, 4).Width = 40;
					table.Cell(txtIndex, 4).Range.Text = "1";
					table.Cell(txtIndex, 4).Range.Font.Name = "Arial";
					table.Cell(txtIndex, 4).Range.Font.Size = 10;
					table.Cell(txtIndex, 4).Range.Bold = 0;
					table.Cell(txtIndex, 4).Range.ParagraphFormat.Alignment =
						Wd.WdParagraphAlignment.wdAlignParagraphLeft;

					table.Cell(txtIndex, 5).Width = 185;
					table.Cell(txtIndex, 5).Range.Text = fi.CreationTime.ToLongTimeString();
					table.Cell(txtIndex, 5).Range.Font.Name = "Arial";
					table.Cell(txtIndex, 5).Range.Font.Size = 10;
					table.Cell(txtIndex, 5).Range.Bold = 0;
					table.Cell(txtIndex, 5).Range.ParagraphFormat.Alignment =
						Wd.WdParagraphAlignment.wdAlignParagraphLeft;

                    //here we forbid break the text across pages
                    //this takes effect only for ONE row
                    range = table.Cell(txtIndex, 1).Range;
                    range.End = table.Cell(txtIndex, 5).Range.End;
                    range.Rows.AllowBreakAcrossPages = 0;

					table.Rows.Add(ref beforeRow);
                    bcIndex = table.Rows.Count;

                    //merge table cells to insert barcode into
                    range = table.Cell(bcIndex, 2).Range;
					range.End = table.Cell(bcIndex, 5).Range.End;
					range.Cells.Merge();

                    //—--8<--- vvv
                    //in 1. cell of a barcode row (not used before)
                    //we place here a field with a page number
                    //that allow us to control new page started
                    range = table.Cell(bcIndex, 1).Range;
                    range.End = range.End - 1;
                    range.Start = range.End;
                    range.Select();
                    Wd.Field fld = WordApp.Selection.Range.Fields.Add(WordApp.Selection.Range, Wd.WdFieldType.wdFieldPage, missing, missing);
                    range = fld.Result;
                    if (pageNr < int.Parse(range.Text))
                    {
                        pageNr = int.Parse(range.Text);
                        splitTable = true;
                    }
                    else
                        splitTable = false;
                    fld.Delete();
                    //—--8<--- ^^^

                    //format data for barcode
                    range = table.Cell(bcIndex, 2).Range;
					range.Text = string.Format("*{0}*", "MS21076-3");//anbauteil.AnbauteilPNR);
					range.Font.Name = "Code-39-25";
					range.Font.Size = 20;
					range.Bold = 0;
					range.ParagraphFormat.Alignment =
					Wd.WdParagraphAlignment.wdAlignParagraphLeft;
                    //—--8<--- vvv
                    //let's set asterisks smaller and readable
                    //left asterisk
                    range = table.Cell(bcIndex, 2).Range;
                    start = range.Start;
                    end = range.Start + 1;
                    range = ak01Doc.Range(ref start, ref end);
                    //Debug.Print("Try to set readable font for {0}", range.Text);
					range.Font.Name = "Arial";
					range.Font.Size = 10;
                    //and now the right one
                    //range of cell ends with \r\a
                    //so we have to select the last character prior to these two
                    range = table.Cell(bcIndex, 2).Range;
                    start = range.End - 2;
                    end = range.End - 1;
                    range = ak01Doc.Range(ref start, ref end);
                    //Debug.Print("Try to set readable font for {0}", range.Text);
					range.Font.Name = "Arial";
					range.Font.Size = 10;
                    //—--8<--- ^^^

                    //—--8<--- vvv
                    if (splitTable)
                    {
                        object oSplitBefore = txtIndex;
                        table.Split(ref oSplitBefore);
                        Debug.Print("{0}. table will be splited after {1} rows", ak01Doc.Tables.Count, table.Rows.Count);
                        table = ak01Doc.Tables[ak01Doc.Tables.Count];
                        Debug.Print("New table {0} starts", ak01Doc.Tables.Count);
                    }
                    //—--8<--- ^^^
                }

                //my adding to save results and quit word
                //to check possible memory leaks
                object oName = "Doc3.docx";
                ak01Doc.SaveAs(ref oName, ref missing, ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref missing, ref missing, ref missing,
                               ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                WordApp.Quit(ref missing, ref missing, ref missing);
			}
			catch (Exception ex)
			{
				Debug.Print("Problem processing document due to:{0}", ex.Message);
			}

        }
    }
}
